<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $posts = DB::table('posts')->get();
    dd($posts);
    return view('index')->with('posts', $posts);
});

/**
 * Posts
 */

Route::get('/posts', 'PostsController@index');
Route::get('/posts/create', 'PostsController@create');
Route::get('/posts/{post}', 'PostsController@show');
Route::put('/posts', 'PostsController@store');
Route::get('/posts/{post}/edit', 'PostsController@edit');
Route::post('/posts/{post}','PostsController@update');
Route::get('/posts/{post}/delete','PostsController@delete');
Route::delete('/posts/{post}', 'PostsController@destroy');

/**
 * GET /books - все записи
 * GET /books/3 - просмотреть запись c id 3
 * PUT /books - создаем новую запись
 * POST /books/3 - обновить запись c id 3
 * DELETE /books/3 - удалить запись c id 3
 */

/** Comments routes */
Route::put('/posts/{post}/comments','CommentsController@store');
/** User routes*/
Route::get('/registration', 'RegistrationController@create');
Route::put('/registration', 'RegistrationController@store');
Route::get('/session/destroy', 'SessionsController@destroy');
Route::get('/session','SessionsController@create');
Route::post('/session','SessionsController@store');

/** Products routes */
Route::resources(['products' => 'ProductsController']);
/** Cart routes */
Route::get('/cart/{product}', 'CartController@store');
Route::get('/cart/{product}/delete', 'CartController@remove');
Route::get('/cart', 'CartController@index');
/** Orders routes */
Route::get('/orders', 'OrdersController@create');
Route::post('/orders', 'OrdersController@store');


/*
 * Dashboard
 */

Route::get('/admin', 'admin\IndexController@index');
Route::get('/admin/orders', 'admin\OrdersController@index');
Route::delete('/admin/orders/{order}/{product}', 'admin\OrdersController@removeProduct');