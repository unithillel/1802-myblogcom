<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            [
                'title' => 'Php is awesome',
                'slug' => 'php_is_awesome',
                'short_description' => 'Article about Php language.',
                'body' => 'Article about Php language. Article about Php language. Article about Php language.'
            ],
            [
                'title' => 'Html is awesome',
                'slug' => 'html_is_awesome',
                'short_description' => 'Article about Php language.',
                'body' => 'Article about Php language. Article about Php language. Article about Php language.'
            ],
            [
                'title' => 'Javascript is awesome',
                'slug' => 'javascript_is_awesome',
                'short_description' => 'Article about Php language.',
                'body' => 'Article about Php language. Article about Php language. Article about Php language.'
            ]
        ]);
    }
}
