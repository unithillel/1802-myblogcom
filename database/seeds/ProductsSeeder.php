<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'title' => 'Php for beginers',
                'slug' => 'php_for_beginers',
                'price' => 20.2,
                'description' => 'Super book for beginers.'
            ],
            [
                'title' => 'Php 7.2',
                'slug' => 'php_7_2',
                'price' => 23.2,
                'description' => '-'
            ],
            [
                'title' => 'Javascript for beginers',
                'slug' => 'js_for_beginers',
                'price' => 13.2,
                'description' => 'Javascript for beginers'
            ]
        ]);
    }
}
