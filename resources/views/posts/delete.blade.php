@extends('layouts.main')

@section('jumbotron')
    <h1 class="display-3">{{$post->title}}</h1>
    <p>{{$post->short_description}}</p>
@endsection

@section('main_content')
    <div class="col-md-12">
        <form action="/posts/{{$post->slug}}" method="post">
            {{method_field('delete')}}
            {{csrf_field()}}
            <h2>Are you sure you want to delete <b>{{$post->title}}</b> ?</h2>
            <div class="form-group">
                <button class="btn btn-danger">yes</button>
                <a class="btn btn-warning" href="/posts">no</a>
            </div>
        </form>
    </div>
@endsection