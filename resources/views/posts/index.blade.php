@extends('layouts/main')

@section('main_content')
    @foreach($posts as $post)
        <div class="col-md-4">
            <h2>{{ $post->title }}</h2>
            <p>{{ $post->short_description }}</p>
            <p><a class="btn btn-secondary" href="/posts/{{$post->slug}}" role="button">View details &raquo;</a></p>
            <p><a class="btn btn-warning" href="/posts/{{$post->slug}}/edit" role="button">Edit &raquo;</a></p>
            <p><a class="btn btn-danger" href="/posts/{{$post->slug}}/delete" role="button">Delete &raquo;</a></p>
        </div>
    @endforeach

@endsection

@section('jumbotron')
    <h1 class="display-3">Posts</h1>
    <p>Hillel blog:</p>
@endsection