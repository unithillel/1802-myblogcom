@extends('layouts.main')

@section('jumbotron')
    <h1 class="display-3">{{$product->title}}</h1>
@endsection

@section('main_content')
    <div class="col-md-12">
        <p><b>Price:</b> {{$product->price}}</p>
        <p>{{$product->description}}</p>
        <p><a class="btn btn-primary" href="/cart/{{$product->slug}}" role="button">Buy</a></p>
    </div>


@endsection