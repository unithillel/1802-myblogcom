<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Order;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function create(){
        $cart = Cart::getCart();
        if(count($cart) < 1){
            return redirect('/products');
        }
        $products = Cart::getCartProducts($cart);
        return view('orders.create', compact('cart','products'));
    }

    public function store(){
        //validate

        $this->validate(request(),[
            'name' => 'required|min:2',
            'email' => 'required|email',
            'phone' => 'required|min:5'
        ]);

        //save order and get order id
        $order = Order::create(request()->all());

        //create relations
        $cart = Cart::getCart();
        foreach ($cart as $product_id => $amount){
            $order->products()->attach($product_id, ['amount' => $amount]);
        }
        //clean cookie and redirect
        return redirect('/products')->withCookie('cart', json_encode([]));
    }
}
